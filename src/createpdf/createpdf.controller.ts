import { Controller, Get, Req, Res, Post, Body } from '@nestjs/common';
import {Response, Request} from 'express';
import {UserData} from './pdfDto/usersData.dto';
import {ApiData} from './pdfDto/usersData.dto';
import {ApiDateReactor, RunId} from './pdfDto/usersData.dto';
import * as ejs from "ejs";
import * as pdfMake from "html-pdf";
import * as mysql from "mysql";

@Controller('createpdf')
export class CreatepdfController {

  @Get('runInf')
  runInf(@Req() req:Request, @Res() res: Response):void{
    var date = new Date(); 
    var day = date.getDate();
    var strDay : string;
    var strMonth : string;
    var strHour : string;
    var strMin : string;
    if (day < 10){
    strDay = String(day);
    strDay = "0" + strDay;
    } else {
    strDay = String(day);
    }
    var month = date.getMonth() + 1;
    if (month < 10){
    strMonth = String(month);
    strMonth = "0" + month;
    } else{
    strMonth = String(month);
    }
    const year = date.getFullYear();
    var hour = date.getHours();
    if (hour < 10){
    strHour = String(hour);
    strHour = "0" + hour;
    } else {
    strHour = String(hour);
    }
    var min = date.getMinutes();
    if (min < 10){
    strMin = String(min);
    strMin = "0" + min;
    } else {
    strMin = String(min);
    }
    var allDate = strHour + ":" + strMin + " " + strDay + "." + strMonth + "." + String(year);
    let id: RunId;
    id = req.query;
    let str:string;
    for (var i = 0; i < id.id.length;i++ ){
      if(i == 0){
        str = id.id[i];
      } else {
        str += "," + id.id[i];
      }
      
    }
    let sql = mysql.createConnection({
      host: "localhost",
      user: "anton",
      password: "1234",
      database: "jiskefet"
    });
    sql.connect(function(err) {
      if (err) {
        console.log(err);
      }
      console.log("Connected!");
      sql.query(`select run_number, time_o2_start, time_trg_start, time_o2_end, time_trg_end from run where run_number in(${str})`, function (err, result) {
        if (err) {
          console.log(err);
        }
        let run_id:string[] = new Array(result.length);
        let time_o2_start:string[] = new Array(result.length);
        let time_trg_start:string[] = new Array(result.length);
        let time_o2_end:string[] = new Array(result.length);
        let time_trg_end:string[] = new Array(result.length);
        for (var i = 0; i < result.length; i ++){
          run_id[i] = result[i].run_number;
          time_o2_start[i] = result[i].time_o2_start;
          time_trg_start[i] = result[i].time_trg_start;
          time_o2_end[i] = result[i].time_o2_end;
          time_trg_end[i] = result[i].time_trg_end;
        }

        const params = {run_id : run_id, time_o2_start: time_o2_start, time_o2_end: time_o2_end, time_trg_start:time_trg_start, time_trg_end:time_trg_end, date: allDate};
        ejs.renderFile(__dirname + '/13.ejs', params, function(err, html) {
          if(err){
              console.log(err);
          }
         
          const options = { format: 'A4', orientation: 'portrait'};
            pdfMake.create(html, options).toBuffer(function(err, buffer){
              if (err){
                res.end("Error");
                console.log(err);
              }
             
      
              res.writeHead(200, {
                "Content-type": "application/pdf",
                "Content-Disposition": 'attachment;filename="RunInfo_{date}.pdf"'.replace('{date}', allDate)
              });
             return res.end(buffer);
      });
      });

      })});

  }

  @Post('downloadPDF')
  makePdf(@Res() res: Response, @Body() userData: UserData): void {
    var date = new Date(); 
    var day = date.getDate();
    var strDay : string;
    var strMonth : string;
    var strHour : string;
    var strMin : string;
    if (day < 10){
    strDay = String(day);
    strDay = "0" + strDay;
    } else {
    strDay = String(day);
    }
    var month = date.getMonth() + 1;
    if (month < 10){
    strMonth = String(month);
    strMonth = "0" + month;
    } else{
    strMonth = String(month);
    }
    const year = date.getFullYear();
    var hour = date.getHours();
    if (hour < 10){
    strHour = String(hour);
    strHour = "0" + hour;
    } else {
    strHour = String(hour);
    }
    var min = date.getMinutes();
    if (min < 10){
    strMin = String(min);
    strMin = "0" + min;
    } else {
    strMin = String(min);
    }
    var allDate = strHour + ":" + strMin + " " + strDay + "." + strMonth + "." + String(year);
    let fname = userData.fname;
    let lname = userData.lname;
    let occupation = userData.occupation;
    let bugReport = userData.bugReport;
    console.log(userData);
    const params = {firstname : fname, lastname: lname, occupation: occupation, bugReport:bugReport, date:allDate};
    ejs.renderFile(__dirname + '/template.ejs', params, function(err, html) {
        if(err){
            console.log(err);
        }
       
        const options = { format: 'A4', orientation: 'portrait'};
          pdfMake.create(html, options).toBuffer(function(err, buffer){
            if (err){
              res.end("Error");
              console.log(err);
            }
           
    
            res.writeHead(200, {
              "Content-type": "application/pdf",
              "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace('{date}', allDate)
            });
           return res.end(buffer);
    });
    });
    
   

  }
  @Get('apiname')
  api(@Req() req:Request, @Res() res: Response): void {
    var date = new Date(); 
    var day = date.getDate();
    var strDay : string;
    var strMonth : string;
    var strHour : string;
    var strMin : string;
    if (day < 10){
    strDay = String(day);
    strDay = "0" + strDay;
    } else {
    strDay = String(day);
    }
    var month = date.getMonth() + 1;
    if (month < 10){
    strMonth = String(month);
    strMonth = "0" + month;
    } else{
    strMonth = String(month);
    }
    const year = date.getFullYear();
    var hour = date.getHours();
    if (hour < 10){
    strHour = String(hour);
    strHour = "0" + hour;
    } else {
    strHour = String(hour);
    }
    var min = date.getMinutes();
    if (min < 10){
    strMin = String(min);
    strMin = "0" + min;
    } else {
    strMin = String(min);
    }
    var allDate = strHour + ":" + strMin + " " + strDay + "." + strMonth + "." + String(year);
    var temper: ApiData;
    temper = req.query;
  console.log(temper);
  var data = temper.data;
  var temp = temper.temp;
  const params = {data: data ,temp: temp};
    ejs.renderFile(__dirname + '/get1_template.ejs', params, function(err, html) {
      const options = { format: 'A4', orientation: 'portrait'};
        const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
          if (err){
            res.end("Error");
            console.log(err);
          }
          
  
          res.writeHead(200, {
            "Content-type": "application/pdf",
            "Content-Disposition": 'attachment;filename="BugReport_{date}.pdf"'.replace("{date}", allDate)
          });
          res.end(buffer);
        });
      });
  }
  @Get('reactorInfo')
  reactorInfo(@Req() req:Request, @Res() res: Response): void {
  function getRandomInt(min: number, max: number) : number{
     return Math.floor(Math.random() * (max - min)) + min;
    }
  var date: ApiDateReactor;
  date = req.query;
  let temp:number[] = new Array(4);
  let lnames:string[] = ["Smith", "Johnson", "Williams", "Jones", 
  "Brown", "Davies", "Miler", "Wilson", "Moore", "Taylor", "Jones", 
  "Thomas", "Evans", "White", "Anderson", "Martin", "Loe", "Hope", "Crowd", "Young"];
  let fnames:string[] = ["Ethan", "Kevin", "Justin", "Matthew", 
  "William", "Christopher", "Anthony", "Ryan", "Nicholas", "David", "Alex", 
  "James", "Josh", "Dilon", "Brandon", "Philip", "Fred", "Tyler", "Thomas", "Caleb"];
  console.log(date);
  
  for (var i = 0; i < 4; i++){
        var a = getRandomInt(200, 900);
        temp[i] = getRandomInt(200, 900);
        console.log(a);
  }
  console.log(temp);
  var worker1: string = fnames[getRandomInt(0, 19)] + " " + lnames[getRandomInt(0, 19)];
  var worker2: string = fnames[getRandomInt(0, 19)] + " " + lnames[getRandomInt(0, 19)];
  var visitor: string = fnames[getRandomInt(0, 19)] + " " + lnames[getRandomInt(0, 19)];
  var chief: string = fnames[getRandomInt(0, 19)] + " " + lnames[getRandomInt(0, 19)];
  var occupationChief: string = "Chief engineer"; 
  var bugReport: string = "No reports";
  const params = {date: date.date, temp: temp, worker1: worker1, worker2:worker2, occupation1:"Student", occupation2:"Student", visitor:visitor, chiefName:chief, chiefOccupation:occupationChief, bugReport:bugReport};
    ejs.renderFile(__dirname + '/11.ejs', params, function(err, html) {
      const options = { format: 'A4', orientation: 'portrait'};
        const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
          if (err){
            res.end("Error");
            console.log(err);
          }
          
  
          res.writeHead(200, {
            "Content-type": "application/pdf",
            "Content-Disposition": 'attachment;filename="InfoReactor_{date}.pdf"'.replace("{date}", date.date)
          });
          res.end(buffer);
        });
      });
  }
  @Get('jobEvaluation')
  jobEv(@Req() req:Request, @Res() res: Response): void {
  
  var date: ApiDateReactor;
  date = req.query;
  function getRandomInt(min: number, max: number) : number{
    return Math.floor(Math.random() * (max - min)) + min;
   }
  let lnames:string[] = ["Smith", "Johnson", "Williams", "Jones", 
  "Brown", "Davies", "Miler", "Wilson", "Moore", "Taylor", "Jones", 
  "Thomas", "Evans", "White", "Anderson", "Martin", "Loe", "Hope", "Crowd", "Young"];
  let fnames:string[] = ["Ethan", "Kevin", "Justin", "Matthew", 
  "William", "Christopher", "Anthony", "Ryan", "Nicholas", "David", "Alex", 
  "James", "Josh", "Dilon", "Brandon", "Philip", "Fred", "Tyler", "Thomas", "Caleb"];
  let worker:string[] = new Array(13);
  let name: string;
  let name2:string;
  console.log(date);
  for (var i = 0; i < 13; i++){
      var a = getRandomInt(0, 19);
      name = fnames[a];
      a = getRandomInt(0, 19);  
      name2 = lnames[a];
      worker[i] = name + " " + name2;
  }
  let chief: string = fnames[getRandomInt(0, 19)] + " " + lnames[getRandomInt(0, 19)];
 
  
  
  
  
  const params = {date: date.date, worker: worker, Chief:chief};
    ejs.renderFile(__dirname + '/12.ejs', params, function(err, html) {
      const options = { format: 'A4', orientation: 'portrait'};
        const pdfDoc = pdfMake.create(html, options).toBuffer(function(err, buffer){
          if (err){
            res.end("Error");
            console.log(err);
          }
          
  
          res.writeHead(200, {
            "Content-type": "application/pdf",
            "Content-Disposition": 'attachment;filename="JobEvaluation_{date}.pdf"'.replace("{date}", date.date)
          });
          res.end(buffer);
        });
      });
  }
}
