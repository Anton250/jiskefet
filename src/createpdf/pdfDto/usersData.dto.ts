export class UserData{
    fname: string;
    lname: string;
    occupation: string;
    bugReport: string;
}
export class ApiData{
    temp: string[];
    data: string[];
}
export class ApiDateReactor{
    date: string;
}
export class RunId{
    id: string[];
}